from torchtyping import TensorType

# Input types
SPEECH_1D_TENSOR = TensorType["batch", "frequencies", "time"]
SPEECH_2D_TENSOR = TensorType["batch", "channel", "frequencies", "time"]

# Output types
POSTERIOR = TensorType["batch", "logits_or_probs"]
