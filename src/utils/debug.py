from typing import Any, Iterable

import torch
import torch.nn as nn


class Debug(nn.Module):
    def __init__(self, msg: str):
        super().__init__()
        self.msg = msg

    def forward(self, input: Any):
        suffix = ""
        if input is not None:
            if isinstance(input, torch.Tensor):
                suffix = f"input_shape={input.shape}"
            elif isinstance(input, Iterable):
                suffix = f"input_shape={input[0].shape}"

            suffix = ", " + suffix

        print(f"[TORCH DEBUG]: {self.msg}{suffix}")
        return input

    def __repr__(self):
        return f"Debug({self.msg})"
