from abc import ABC
from typing import List
from dataclasses import dataclass, field

import torch.nn as nn
import torch.nn.functional as F
from einops.layers.torch import Rearrange

from nn.ode import NODEConfig
from utils.typing import SPEECH_2D_TENSOR, POSTERIOR


class ResidualBlock(nn.Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        stride: int = 1,
        downsample: nn.Module = None,
        skip_connect: bool = True,
    ):
        super().__init__()

        self.conv1 = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=stride, padding=1),
            nn.BatchNorm2d(out_channels),
            nn.Conv2d(out_channels, out_channels, kernel_size=3, stride=1, padding=1),
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(out_channels, out_channels, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(out_channels),
        )
        self.downsample = downsample
        self.skip_connect = skip_connect

    def forward(self, x: SPEECH_2D_TENSOR) -> SPEECH_2D_TENSOR:
        residual = x

        out = self.conv1(x)
        out = self.conv2(out)

        if self.skip_connect:
            if self.downsample:
                residual = self.downsample(x)

            out += residual
            out = F.relu(out, inplace=True)

        return out


@dataclass
class ResNetConfig:
    n_channels: int = 3
    n_classes: int = 10
    block: nn.Module = ResidualBlock
    layers: List[int] = field(default_factory=lambda: [3, 4, 6, 3])


class ResNet(nn.Module):
    model: nn.Module

    def __init__(self, config: ResNetConfig):
        super().__init__()

        self._layers = config.layers
        self._planes = [64, 128, 256, 512]
        self._strides = [1, 2, 2, 2]
        self._in_planes = self._planes[0]

        self._init_model(config.n_channels, config.block, config.n_classes)

    def _init_model(self, in_channels: int, block: nn.Module, num_classes: int):
        init_conv = nn.Sequential(
            nn.Conv2d(in_channels, self._in_planes, kernel_size=7, stride=2, padding=3),
            nn.BatchNorm2d(self._in_planes),
            nn.ReLU(),
        )

        self.model = nn.Sequential(
            init_conv,
            nn.MaxPool2d(kernel_size=3, stride=2, padding=1),
            self._make_layer(block, 0),
            self._make_layer(block, 1),
            self._make_layer(block, 2),
            self._make_layer(block, 3),
            nn.AvgPool2d(7, stride=1),
            Rearrange("b c h w -> b (c h w)"),
            nn.Linear(512, num_classes),
        )

    def _make_layer(self, block: nn.Module, block_id: int) -> nn.Module:
        planes = self._planes[block_id]
        stride = self._strides[block_id]
        n_blocks = self._layers[block_id]

        downsample = None
        if stride != 1 or planes != self._in_planes:
            downsample = nn.Sequential(
                nn.Conv2d(self._in_planes, planes, kernel_size=1, stride=stride),
                nn.BatchNorm2d(planes),
            )

        init_layer = block(self._in_planes, planes, stride, downsample)
        self._in_planes = planes

        layers = [init_layer]
        for _ in range(1, n_blocks):
            layers.append(block(self._in_planes, planes))

        layer = nn.Sequential(*layers)

        return layer

    def forward(self, x: SPEECH_2D_TENSOR) -> POSTERIOR:
        return self.model(x)


class ResNetNODE(ResNet):
    def __init__(self, resnet_config: ResNetConfig, node_config: NODEConfig):
        self.node_cfg = node_config
        super().__init__(resnet_config)

    def _make_layer(self, block: nn.Module, block_id: int) -> nn.Module:
        planes = self._planes[block_id]
        stride = self._strides[block_id]

        downsample = None
        if stride != 1 or planes != self._in_planes:
            downsample = nn.Sequential(
                nn.Conv2d(self._in_planes, planes, kernel_size=1, stride=stride),
                nn.BatchNorm2d(planes),
            )

        init_layer = block(self._in_planes, planes, stride, downsample)
        self._in_planes = planes

        vf = block(self._in_planes, planes, skip_connect=False)
        node = self.node_cfg.instantiate_ode_block(vf)
        layer = nn.Sequential(init_layer, node)

        return layer
