from dataclasses import dataclass

import torch
import torch.nn as nn
from torchdyn.core import NeuralODE


class TimePreparedNeuralODE(NeuralODE):
    def __init__(self, t_span: torch.Tensor, **kwargs):
        super().__init__(**kwargs)
        self.t_span = t_span

    def forward(self, x: torch.Tensor):
        return super().forward(x, self.t_span)


@dataclass
class NODEConfig:
    sensitivity: str = "autograd"
    order: int = 1

    # Forward dynamics setting
    solver: str = "tsit5"
    atol: float = 1e-3
    rtol: float = 1e-3

    # Backward dynamics settings
    solver_adjoint: str = None
    atol_adjoint: float = 1e-4
    rtol_adjoint: float = 1e-4

    # Spanning in time
    t_span: torch.Tensor = None

    def instantiate_ode_block(self, vector_field: nn.Module, t_span: torch.Tensor = None) -> NeuralODE:
        assert t_span is not None or self.t_span is not None, "t_span should be defined"

        return TimePreparedNeuralODE(
            t_span if t_span is not None else self.t_span,
            vector_field=vector_field,
            solver=self.solver,
            order=self.order,
            atol=self.atol,
            rtol=self.rtol,
            sensitivity=self.sensitivity,
            solver_adjoint=self.solver_adjoint,
            atol_adjoint=self.atol_adjoint,
            rtol_adjoint=self.rtol_adjoint,
        )
