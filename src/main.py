# Torch
import torch
import torch.nn as nn
from torch.utils.data import TensorDataset, random_split, DataLoader

# Torchdyn
from torchdyn.core import NeuralODE
from torchdyn.datasets import ToyDataset

# TorchLightning
from pytorch_lightning import Trainer, LightningModule


DEVICE = "cuda"


class Learner(LightningModule):
    def __init__(self, t_span: torch.Tensor, model: nn.Module):
        super().__init__()
        self.model, self.t_span = model, t_span

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):
        self.model.vf.nfe = 0
        x, y = batch
        t_eval, y_hat = self.model(x, self.t_span)
        y_hat = y_hat[-1]
        loss = nn.CrossEntropyLoss()(y_hat, y)

        self.log_dict({"loss": loss, "nfe": self.model.vf.nfe}, prog_bar=True)
        return {"loss": loss}

    def test_step(self, batch, batch_idx: int):
        x, y = batch
        t_eval, y_hat = self.model(x, self.t_span)
        loss = nn.CrossEntropyLoss()(y_hat, y)

        return loss

    def configure_optimizers(self):
        return torch.optim.Adam(self.model.parameters(), lr=0.01)


def main():
    # Define the model
    block = nn.Sequential(
        nn.Linear(2, 16),
        nn.Tanh(),
        nn.Linear(16, 2),
    )
    t_span = torch.linspace(0, 1, 20)
    node_block = NeuralODE(
        block,
        sensitivity="adjoint",
        solver="rk4",
        solver_adjoint="dopri5",
        atol_adjoint=1e-4,
        rtol_adjoint=1e-4,
    ).to(DEVICE)
    model = Learner(t_span, node_block)

    # Define dataset
    X, y = ToyDataset().generate(n_samples=1024, dataset_type="moons", noise=1e-2, dim=10)
    dset = TensorDataset(X.to(DEVICE), y.to(DEVICE))
    train_dset, test_dset, val_dset = random_split(dset, lengths=[0.8, 0.1, 0.1])

    # Define loaders
    train_loader = DataLoader(train_dset, batch_size=64, shuffle=True)
    test_loader = DataLoader(test_dset)
    val_loader = DataLoader(val_dset)

    # Define trainer
    trainer = Trainer(min_epochs=30)
    trainer.fit(model, train_loader, val_loader)
    trainer.test(model, test_loader)


if __name__ == "__main__":
    main()
