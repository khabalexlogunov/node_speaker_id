import torch
import torch.nn as nn
from torch.utils.data import random_split, DataLoader
from torchaudio.datasets import VoxCeleb1Identification
from pytorch_lightning import Trainer, LightningModule

from utils.typing import SPEECH_2D_TENSOR
from nn.ode import NODEConfig
from nn.resnet import ResNetNODE, ResNetConfig, ResNet


class Learner(LightningModule):
    def __init__(self, model: nn.Module):
        super().__init__()
        self.model = model

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch: SPEECH_2D_TENSOR, batch_idx: int):
        x, _, y, _ = batch
        t_eval, y_hat = self.model(x)
        y_hat = y_hat[-1]
        loss = nn.CrossEntropyLoss()(y_hat, y)

        self.log_dict({"loss": loss}, prog_bar=True)
        return {"loss": loss}

    def test_step(self, batch, batch_idx: int):
        x, _, y, _ = batch
        t_eval, y_hat = self.model(x)
        loss = nn.CrossEntropyLoss()(y_hat, y)

        return loss

    def configure_optimizers(self):
        return torch.optim.Adam(self.model.parameters(), lr=0.01)


def main():
    # Init data
    train_dset = VoxCeleb1Identification("./datasets", subset="train", download=True)
    test_dset = VoxCeleb1Identification("./datasets", subset="test", download=True)
    train_loader = DataLoader(train_dset, batch_size=64, shuffle=True)
    test_loader = DataLoader(test_dset)

    # Init model
    rn_cfg = ResNetConfig(1, 10)
    node_cfg = NODEConfig(
        sensitivity="adjoint",
        solver="rk4",
        solver_adjoint="dopri5",
        t_span=torch.linspace(0, 1, steps=5),
    )
    # rn_model = ResNetNODE(rn_cfg, node_cfg)
    rn_model = ResNet(rn_cfg)
    model = Learner(rn_model)

    # Init trainer
    trainer = Trainer(min_epochs=30)

    # Run training
    trainer.fit(model, train_loader)
    trainer.test(model, test_loader)


if __name__ == "__main__":
    main()
